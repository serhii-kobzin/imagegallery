//
//  User.swift
//  ImageGallery
//
//  Created by Serhii Kobzin on 5/21/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import Foundation


class User {
    
    var name: String?
    var email: String
    var password: String
    var avatar: Data
    
    init(name: String?, email: String, password: String, avatar: Data) {
        self.name = name
        self.email = email
        self.password = password
        self.avatar = avatar
    }
    
}
