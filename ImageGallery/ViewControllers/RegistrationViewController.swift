//
//  RegistrationViewController.swift
//  ImageGallery
//
//  Created by Serhii Kobzin on 5/21/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonTouchUpInside(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
