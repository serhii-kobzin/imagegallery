//
//  NetworkManager.swift
//  ImageGallery
//
//  Created by Serhii Kobzin on 5/21/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import Foundation


enum requestResult {
    case success
    case error(description: String)
}


class NetworkManager {
    
    static let shared = NetworkManager()
    
    private let baseURL = "http://api.doitserver.in.ua"
    private let createURL = "/create"
    private let loginURL = "/login"
    
    func login(email: String, password: String, completionHandler: @escaping (requestResult) -> ()) {
        let parameters = ["email"    : email,
                          "password" : password]
        guard let url = URL(string: baseURL + createURL), let uploadData = try? JSONEncoder().encode(parameters) else {
            completionHandler(.error(description: "Wrong request parameters"))
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = uploadData
        URLSession.shared.dataTask(with: url, completionHandler: { _, response, error in
            guard error == nil else {
                completionHandler(.error(description: "Some error while login request"))
                return
            }
            guard let response = (response as? HTTPURLResponse), response.statusCode == 200 else {
                completionHandler(.error(description: "Wrong email or password"))
                return
            }
            completionHandler(.success)
        }).resume()
    }
    
}
